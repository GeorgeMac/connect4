# Connect 4

### Simple two player connect 4 server using Ruby (Sinatra) and WebSockets

- - -

How to get it up and running:

    > git clone git@bitbucket.org:GeorgeMac/connect4.git
    > bundle install
    > rackup -p 5000

- Visit http:localhost:5000
- Click the "Generate New Game" button!
- Share the link with a friend!

Tech Stack:

 - Ruby Rack/Sinatra web application.
 - Ruby sinatra websockets for two way communication between the server and the client.
 - Javascript for client side request/response handling and board rendering.
 - JQuery for convenient dom traversal and animation helper methods. 
 - HTML5 / CSS3
