require 'json'

module Connect4
	class Game
		attr_accessor :configuration, :winningMoves

		MAX_COLS = 7
		MAX_ROWS = 6

		COLUMN_OUT_OF_BOUNDS = [1]
		COLUMN_FULL = [2]

		def initialize()
			@configuration = Array.new(7) { Array.new }
			@winningMoves = nil
		end

		def addRed(column)
			return insert column, 'r'
		end

		def addYellow(column)
			return insert column, 'y'
		end

		def addColorToColumn(column, color)
			return insert(column, (color.zero? ? 'r' : 'y'))
		end

		def prettyConfigString()
			pretty = "<div class=\"board\">"
			configuration.each do |column|
				pretty << "<div class=\"column\">"
				(0..5).to_a.reverse.each do |index|
					row = column[index]
				    pretty << "<div class=\"row\">"
				    if row.nil?
				    	pretty << " o "
				    else
				    	pretty << " #{row} "
				    end
				    pretty << "</div>"
			    end
				pretty << "</div>"
			end
			pretty << "</div>"
		end

		private
		def insert(column, piece)
			if @winningMoves.nil?
				columnArray = configuration[column]
				return COLUMN_OUT_OF_BOUNDS unless (0..configuration.length-1).include?(column)
				return COLUMN_FULL if columnArray.length == 6
				columnArray << piece
				return evaluate(column, columnArray.length-1)
			end
			return [4, @winningMoves]
		end

		def evaluate(col, row)
			type = configuration[col][row]
			matches = []
			Vectors.ALL.each do | pair |
				match = [[col, row]]
				pair.each do | vector |
					getMatchesInDirection(type, match[0], vector, []).each do |m|
						match << m
					end
				end
				matches << match
			end

			matches.each { |m|  @winningMoves = m if m.length >= 4 }
		  return [0, [col, row]] if @winningMoves.nil?
		  return [4, [col, row], @winningMoves]
		end

		def getMatchesInDirection(type, vector, direction, matches)
			nextVector = translate(vector, direction)
			return matches if !(0..(configuration.length-1)).include?(nextVector[0]) || !(0..5).include?(nextVector[1])
			nextType = configuration[nextVector[0]][nextVector[1]]
			return matches if nextType.nil? || nextType != type
			matches << nextVector
			return getMatchesInDirection(type, nextVector, direction, matches)
		end

		def translate(initialVector, translationVector)
			initialVector.zip(translationVector).map { |pair| pair.reduce(:+) }
		end

		class Vectors
			@directions = (-1..1).to_a.repeated_permutation(2).reject { |pair| pair == [0, 0] }

			def self.NORTHEAST_SOUTHWEST
				self.get(0)
			end

			def self.EAST_WEST
				self.get(1)
			end

			def self.NOTHWEST_SOUTHEAST
				self.get(2)
			end

			def self.NORTH_SOUTH 
				self.get(3)
			end

			def self.ALL
				4.times.map { |i| self.get(i) }
			end

			private
			def self.get(index)
				[@directions[index], @directions[@directions.length - index - 1]]
			end
		end
	end
end