require './modules/game.rb'

module Connect4
	class GameSession
		attr_accessor :players, :game, :current, :mutex

		def initialize()
			@game = Connect4::Game.new
			@players = Array.new(2,nil)
			@current = 0
			@mutex = Mutex.new
		end

		def getNewPlayer(socket)
			@mutex.synchronize {
				  return nil unless !isFull?()
			    team = @players.index(nil)
		      @players[team] = Player.new(socket, color=team)
		    }
		end

		def removePlayer(socket)
			@mutex.synchronize {
				player = getPlayerBySocket socket
			  @players[player.color] = nil unless player.nil?
			}
		end

		def full?()
			@mutex.synchronize {
			    return isFull?()
			}
		end

		def makeMove(socket, move)
			@mutex.synchronize {
				return false unless isFull?
				player = getPlayerBySocket socket
				return false unless player.socket == @players[@current].socket
				state = @game.addColorToColumn(move, player.color)
				notify(state, player.color)
				switch()
				return true
			}
		end

		def getOpponent(mySocket)
			@mutex.synchronize {
			  @players.reject { |x| x.nil? || x.socket == mySocket }[0]
			}
		end

		private
		def notify(state, color)
			if(state.is_a?(Integer) && state.zero?)
				@players.each do |p|
					p.socket.send(JSON.generate({ status: state[0], piece: color }))
				end
			elsif (state.is_a?(Array))
				@players.each do |p|
					response = { status: state[0], data: state[1], piece: color }
					response["solution"] = state[2] unless state[2].nil?
					response["message"] = (p.color == @players[@current].color ? "Congratulations!" : "Better Luck Next Time!") unless state[0] != 4
					p.socket.send(JSON.generate(response))
				end
			end
		end

		def getPlayerBySocket(socket)
			@players.reject { |x| x.nil? || x.socket != socket }[0]
		end

		def switch()
		  @current = @current.zero? ? 1 : 0
		end

		def isFull?()
			@players.index(nil).nil?
		end

	end

	class Player
		attr_accessor :socket, :color
		def initialize(socket=nil, color=0)
			@socket = socket
			@color = color
		end
	end
end