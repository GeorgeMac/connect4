require 'sinatra/base'
require 'sinatra-websocket'
require 'json'
require './modules/session.rb'

module Connect4
	class App < Sinatra::Base

		enable :sessions

		set :gameSessions, {}
		set :public_folder, File.dirname(__FILE__) + '/static'

		get "/" do
			@newGameId = (session[:session_id].chars.map { |b| ((rand*100).floor * b.bytes[0]) }).reduce { |sum, x| sum + x }
            haml :"index.html", :format => :html5
		end

		get "/favicon.ico" do
			[200, {}, []]
	  end

		get "/game/:id" do |id|
			settings.gameSessions[id] = Connect4::GameSession.new if settings.gameSessions[id].nil?
			sesh = settings.gameSessions[id]

			if !request.websocket?
				if !sesh.full?
					@id = id
					@game = sesh.game
					haml :"game.html", :format => :html5
				else
					[403, {"Content-Type" => "text/html"}, ["Forbidden, this Game is full!"]]
				end
			else
				request.websocket do |ws|
					ws.onopen do
						ws.send(JSON.generate({status:0, message:"Connected", piece: "1"}))
						ws.send(JSON.generate({status:-1, message: "Sorry We're Full"})) unless sesh.getNewPlayer(ws)
						if sesh.full?
							opponent = sesh.getOpponent(ws)
							opponent.socket.send(JSON.generate({status:0, message:"Make your move, you have an opponent!", piece:"1"}))
						else
							ws.send(JSON.generate({status:-1, message:"Please wait for another player to join"}))
						end
					end
					ws.onmessage do |msg|
						ws.send(JSON.generate({status: -1, message: "Please wait it is not your turn"})) unless sesh.makeMove(ws, msg.to_i)
					end
					ws.onclose do
						opponent = sesh.getOpponent(ws)
						opponent.socket.send(JSON.generate({status:-1, message:"Your opponent has left I'm afraid"})) unless opponent.nil?
						sesh.removePlayer ws
					end
				end
			end
		end
	end
end
