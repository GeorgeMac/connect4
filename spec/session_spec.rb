require "./modules/session.rb"
require 'rspec'
require 'rack/test'
require 'json'

describe Connect4::GameSession do
	before(:each) do
		@sesh = Connect4::GameSession.new
	end

	it "Should be able to add a player" do
		player = @sesh.getNewPlayer(mockSocket)
		player.should be_an_instance_of Connect4::Player
		storedPlayer = @sesh.players[0]
		storedPlayer.should be_an_instance_of Connect4::Player
		player.should === storedPlayer
	end

	it "Shouldn't be able to store more than two players" do
		2.times { @sesh.getNewPlayer(mockSocket) }
		@sesh.full?.should == true
		@sesh.getNewPlayer(mockSocket).should == nil
		@sesh.full?.should == true
		@sesh.players.size().should == 2
	end

	it "Should return false on the makeMove method when there aren't enough users" do
		@sesh.makeMove(mockSocket, 0).should == false
		@sesh.getNewPlayer(mockSocket)
		@sesh.makeMove(mockSocket, 0).should == false
	end

	context "There are enough players in the game" do
		before(:each) do
			@player1 = @sesh.getNewPlayer(mockSocket)
			@player2 = @sesh.getNewPlayer(mockSocket)
		end

		it "Should return true on the makeMove method when there are enough users" do
			@sesh.makeMove(@player1.socket, 0).should == true
		end

		it "Shouldn't let a player make a move until it is their turn" do
			@sesh.makeMove(@player1.socket, 0).should == true
			@sesh.makeMove(@player1.socket, 0).should == false
			@sesh.makeMove(@player2.socket, 0).should == true
			@sesh.makeMove(@player2.socket, 0).should == false
		end

		it "Should notify both players if and when a legal move is played with a status of 0" do
			@sesh.makeMove(@player1.socket, 0).should == true
			@player1.socket[:log][0]["status"].should == 0
			@player2.socket[:log][0]["status"].should == 0
		end

		it "Should notify both players if and when a legal move is played with the coords of the move just made" do
			@sesh.makeMove(@player1.socket, 0).should == true
			data = @player1.socket[:log][0]["data"]
			data.should be_an_instance_of Array
			data[0].should == 0
			data[1].should == 0
			data = @player2.socket[:log][0]["data"]
			data.should be_an_instance_of Array
			data[0].should == 0
			data[1].should == 0
		end
	end
end

def mockSocket()
	socket = {log:[], id: rand}
	socket.stub(:send) do |data|
		socket[:log] << JSON.parse(data)
	end
	return socket
end