require "./modules/game.rb"
require 'rspec'
require 'rack/test'

describe Connect4::Game do

	before(:each) do
		@game = Connect4::Game.new
	end

	it "Should have a blank board on construction" do
		@game.configuration.each { |ary| ary.should == [] }
	end

	it "Should have a single red counter in column 4 after addRed(4) is called" do
		@game.addRed(4)
		@game.configuration[4][0].should == 'r'
	end

  ## Board Config
	# o o o o o o o
	# o o o o o o o
	# o o o o o o o
	# o o o o o o o
	# o o o o o o o
	#(r|r|r|r)o o o
	##
	it "Should return a correct response when a player performs a winning move" do
		response = []
		4.times { |i| response = @game.addRed i }
		response[0].should == 4
		response[1].should be_an_instance_of Array
		response[2].should be_an_instance_of Array
	end

	## Board Config
	# o o o o o o o
	# o o o o o o o
	# o o o o o o o
	# o o o o o o o
	# o o o o o o o
	#(r|r|r|r|r)o o
	##
	it "Should accept 4 or more pieces in a row as a valid solution" do
		[0,1,3,4].each { |i| @game.addRed i }
		response = @game.addRed 2
		response[0].should == 4
		response[2].should be_an_instance_of Array
		response[2].size.should == 5
	end

	## Board Config
	# o o o o o o o
	# o o o o o o o
	#(r)o o o o o o
	#(r)o o o o o o
	#(r)o o o o o o
	#(r)o o o o o o
	##
	it "Should be able to find verticle solutions" do
		response = []
		4.times { |i| response = @game.addRed 0 }
		response[0].should == 4
	end

	## Board Config
	# o o o o o o o
	# o o o o o o o
	# o o o(r)o o o
	# o o(r)y o o o
	# o(r)y y o o o
	#(r)y y y o o o
	##
	it "Should be able to find diagonal solutions and return the solution pieces" do
		response = []
		3.times { |i| @game.addYellow(i+1) }
		2.times { |i| @game.addYellow(i+2) }
		@game.addYellow(3)
		4.times { |i| response = @game.addRed i }
		response[0].should == 4
		response[2].each do |point|
			@game.configuration[point[0]][point[1]].should == 'r'
		end
	end

end