var uri = "ws://" + window.document.location.host + window.document.location.pathname;
console.log("Connecting to: " + uri);
var ws = new WebSocket(uri);
var information = $("#info");
var columns = $(".c4-board .c4-column");
var board = new Array(columns.size());

var playersTokens = [$($(".token")[0]),$($(".token")[1])];

columns.each(function(colindex, column) {
	$(column).click(function(){
		ws.send(colindex);
	}).mouseover(function(event){
		$(event.currentTarget).css({"background-color": "green"});
	}).mouseout(function(event){
		$(event.currentTarget).removeAttr("style");
	});
	rows = $($(".c4-row", column).get().reverse());
	board[colindex] = new Array(rows.size());
	rows.each(function(rowindex, piece){
		board[colindex][rowindex] = piece;
		$(piece).bind("place", function(event, color){
			target = $(event.currentTarget)
			targetOffset = target.offset()
			boardOffset = target.parents(".c4-board").offset()

			offset = boardOffset.top - targetOffset.top - 300
			color == 0 ? target.append(generateRedPiece(offset)) : target.append(generateYellowPiece(offset))
			$("svg[style]", target).animate({ top: "-55" });
		});
	});
});

ws.onmessage = function(message) {
	response = JSON.parse(message.data)
	switch(response.status) {
		case 0:
			moveIfPossible(response);
			handlePlayer(response);
	    break;
		case 4:
	    moveIfPossible(response);
	    showSolution(response);
	    break;
		default:
	    break;
	}
	handleMessage(response);
};

function moveIfPossible(response) {
	if(response["data"]) {
		coords = response.data;
    $(board[coords[0]][coords[1]]).trigger("place",response.piece);
	}
};

function showSolution(response) {
	solution = response.solution;
	for (pos in solution) {
		coords = solution[pos];
		piece = $(board[coords[0]][coords[1]]);
		piece.prepend("<div class=\"winning-container\"><div class=\"winning-piece\" /></div>");
		$(".winning-piece",piece).animate({opacity: 1},5000);
	}
};

function handleMessage(response) {
	if (response["message"]) {
		alerts = $(".alert", information);
		if (alerts.size() > 2) {
			alerts.first().remove();
		}
		statusColor = "";
		switch (response.status) {
			case 4:
			  statusColor = "alert-success";
			  break;
			case -1:
			  statusColor = "alert-error";
		}
		information.append("<div class=\"alert "+ statusColor +" fade in\">" + response.message + "</div>");
	}
};

function handlePlayer(response) {
	redSwitch = response.piece == 1 ? "red" : "white";
	yellowSwitch = response.piece == 0 ? "yellow" : "white";
	playersTokens[0].css("background-color",redSwitch);
	playersTokens[1].css("background-color",yellowSwitch);
};

function generateRedPiece(offset) {
	return generatePiece("red-grad", "#FA9898", "#F05D5D", "#F05D5D", offset)
};

function generateYellowPiece(offset) {
	return generatePiece("yellow-grad", "#FFFEDE", "#EDEA93", "#EDE60C", offset)
}

function generatePiece(id, low, mid, hi, offset) {
	return "<svg height=\"50\" width=\"50\" xmlns=\"http://www.w3.org/2000/svg\" style=\"top: "+ offset +"\"> \
                <defs> \
                  <radialGradient cx=\".3\" cy=\".3\" id=\""+ id +"\" r=\".8\"> \
                    <stop offset=\"0\" stop-color=\""+ low + "\"></stop> \
                    <stop offset=\".3\" stop-color=\""+ mid +"\"></stop> \
                    <stop offset=\"1\" stop-color=\""+ hi +"\"></stop> \
                  </radialGradient> \
                </defs> \
                <circle cx=\"25\" cy=\"25\" fill=\"url(#"+ id +")\" r=\"20\"></circle> \
              </svg>"
};
